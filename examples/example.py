from earoots import roots

sols, radii = roots([1, 0, -1], radius=True)

print("Solutions:")
for s,r in zip(sols, radii):
    print("  {0: .15e} + I {1: .15e}     +- {2:.3e}".format(s.real, s.imag, r))
