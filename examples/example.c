#include <stdio.h>
#include <complex.h>
#include <earoots.h>

int main() {
    double complex poly[] = {1, 0, -1};
    double crads[] = {0.001, 0.001, 0.001};
    double complex result[2];
    double radii[2];

    ea_roots((double*) poly, crads, 3, 500, 0x1p-25, 0, (double*) result, radii);


    double complex points[] = {1, -1, I, 1+I};
    double complex z[] = {0, 0, 0, 0};
    double r[] = {0, 0, 0, 0};
    ea_evaluate((double*) poly, crads, 3, (double*) points, 4, 0x1p-53, (double*) z, r);

    printf("Solutions:\n");
    printf("  % .15e + I % .15e     +- %.3e\n", creal(result[0]), cimag(result[0]), radii[0]);
    printf("  % .15e + I % .15e     +- %.3e\n", creal(result[1]), cimag(result[1]), radii[1]);

    printf("Evaluations:\n");
    printf("  % .15e + I % .15e     +- %.3e\n", creal(z[0]), cimag(z[0]), r[0]);
    printf("  % .15e + I % .15e     +- %.3e\n", creal(z[1]), cimag(z[1]), r[1]);
    printf("  % .15e + I % .15e     +- %.3e\n", creal(z[2]), cimag(z[2]), r[2]);
    printf("  % .15e + I % .15e     +- %.3e\n", creal(z[3]), cimag(z[3]), r[3]);

}


