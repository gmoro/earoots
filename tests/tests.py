import earoots as ea
import numpy as np
from scipy.spatial import cKDTree
from scipy import special as sp
from matplotlib import pyplot as plt
import sys
import unittest
import warnings

# Polynomial families
def p_multiple_zeros(v, d):
    return np.array([0]*v + [1] + [0]*(d-v-1) + [1])

def p_multiple_ones(d):
    p = np.zeros(d+1)
    p[:2] = [1, -1]
    for k in range(d-1):
        p[1:] -= p[:-1] 
    return p

def p_random_normal_real(d):
    np.random.seed(0)
    p = np.random.normal(size=d+1)
    return p

def p_random_normal_complex(d):
    np.random.seed(0)
    p = np.random.normal(size=d+1) + 1j*np.random.normal(size=d+1)
    return p

def p_mignotte(d):
    p = np.array([-2, 8, -8] + [0]*(d-3) + [1])
    return p

def p_truncated_exponential(d):
    p = 1/sp.factorial(np.arange(d+1))
    return p

def p_leading_zero(d):
    p = np.array([1] + [0]*d)
    return p

def p_leading_small(e):
    p = np.array([1,e])
    return p

def p_all_zeros(d):
    p = np.zeros(d+1)
    return p

def p_small_coeffs(d, e):
    p = np.array([e]+[0]*(d-1)+[1/e])
    return p

def p_linear(n):
    p = np.array([1,1])
    return p

# Plotting functions
def min_e(a):
    return min(a) - abs(min(a))*0.1

def max_e(a):
    return max(a) + abs(max(a))*0.1

def plot_sols(sols, rads, big=None, small=0.002):
    plt.xlim([min_e(sols.real-rads), max_e(sols.real+rads)])
    plt.ylim([min_e(sols.imag-rads), max_e(sols.imag+rads)])
    h = max(max_e(sols.real) - min_e(sols.real), max_e(sols.imag) - min_e(sols.imag))
    if big==None:
        big = 2*h
    plt.gca().set_aspect('equal')
    for s, r in zip(sols, rads):
        if not np.isfinite(r):
            r = big*h
        c = plt.Circle((s.real, s.imag), min(big*h, max(small*h, r)))
        plt.gca().add_patch(c)
    plt.show()

# Test functions
def in_disks(z, centers, radii):
    return np.any(np.abs(centers - z) < radii)

def all_in_disks(Lz, centers, radii):
    Q = cKDTree(centers.view(float).reshape(-1,2))
    d, i = Q.query(Lz.view(float).reshape(-1,2))
    res = np.all(d < radii[i])
    #print(d[d>=radii[i]])
    return res

def all_separated(centers, radii):
    points = centers.view(float).reshape(-1,2)
    T = cKDTree(points)
    pairs = T.query_pairs(2*radii.max())
    for (i,j) in pairs:
        if np.abs(centers[j] - centers[i]) <= radii[i] + radii[j]:
            return False
    return True


class TestEARoots(unittest.TestCase):
    def assertSameRoots(self, p, name, num, w=1, in_radius=None):
        sref = np.polynomial.polynomial.polyroots(p).astype(complex)
        s, r = ea.roots(p, in_rads=in_radius, radius=True)
        if not all_in_disks(sref, s, w*r):
            raise self.failureException(f"Polynomial {name}_{num}: disks returned by earoots don't contain all the roots returned by polyroots")

    def assertAllSeparated(self, centers, radii, name, num):
        if not all_separated(centers, radii):
            raise self.failureException(f"Polynomial {name}_{num}: disks overlap")

    def assertWarningTolerance(self, Lw):
        L = ["tolerance" not in str(w.message) for w in Lw]
        if all(L):
            raise self.failureException("Warning not raised when tolerance not reached")

    def assertNoWarning(self, Lw):
        L = ["tolerance" not in str(w.message) for w in Lw]
        if len(Lw) > 0:
            raise self.failureException("Warning raised")
    
    def assertWarningArithmetic(self, Lw):
        L = ["overflow" not in str(w.message) for w in Lw]
        if all(L):
            raise self.failureException("Warning not raised when overflow")

    def test_multiple_zeros(self):
        p = p_multiple_zeros(3,5)
        s, r = ea.roots(p, radius=True)
        n = np.count_nonzero(r == 0)
        self.assertEqual(n, 3)
        p = p_multiple_zeros(5,10)
        s, r = ea.roots(p, radius=True)
        n = np.count_nonzero(r == 0)
        self.assertEqual(n, 5)

    def test_multiple_ones(self):
        p = p_multiple_ones(2)
        with warnings.catch_warnings(record=True) as Lw:
            s, r = ea.roots(p, radius=True)
            np.testing.assert_almost_equal(s, 1)
            self.assertWarningTolerance(Lw)

    def test_random_real_50(self):
        p = p_random_normal_real(50)
        self.assertSameRoots(p, "random real", 50)

    def test_random_real_100(self):
        p = p_random_normal_real(100)
        self.assertSameRoots(p, "random real", 100)

    def test_random_real_500(self):
        p = p_random_normal_real(500)
        self.assertSameRoots(p, "random real", 500)

    def test_random_real_1000(self):
        p = p_random_normal_real(1000)
        self.assertSameRoots(p, "random real", 1000)

    def test_random_complex(self):
        p = p_random_normal_complex(50)
        self.assertSameRoots(p, "random complex", 50)
        p = p_random_normal_complex(100)
        self.assertSameRoots(p, "random complex", 100)
        p = p_random_normal_complex(500)
        self.assertSameRoots(p, "random complex", 500)

    def test_mignotte(self):
        p = p_mignotte(3)
        self.assertSameRoots(p, "mignotte", 3)
        p = p_mignotte(30)
        self.assertSameRoots(p, "mignotte", 30)
        p = p_mignotte(300)
        with warnings.catch_warnings(record=True) as Lw:
            s, r = ea.roots(p, radius=True)
            self.assertWarningTolerance(Lw)
        n = np.count_nonzero(r < 2**-20)
        self.assertEqual(n, 300-2)

    def test_truncated_exponential(self):
        p = p_truncated_exponential(10)
        self.assertSameRoots(p, "truncated exponential", 10)
        p = p_truncated_exponential(27)
        with warnings.catch_warnings(record=True) as Lw:
            self.assertSameRoots(p, "truncated exponential", 27)
            self.assertNoWarning(Lw)
        p = p_truncated_exponential(28)
        with warnings.catch_warnings(record=True) as Lw:
            self.assertSameRoots(p, "truncated exponential", 28)
            self.assertNoWarning(Lw)
        p = p_truncated_exponential(56)
        with warnings.catch_warnings(record=True) as Lw:
            s, r = ea.roots(p, radius=True)
            self.assertWarningTolerance(Lw)
        self.assertAllSeparated(s, r, 'truncated exponential', 56)

    def test_all_zeros(self):
        p = p_all_zeros(3)
        with warnings.catch_warnings(record=True) as Lw:
            s, r = ea.roots(p, radius=True)
            self.assertWarningTolerance(Lw)
        n = np.count_nonzero(~np.isfinite(r) | (np.minimum(r, r/np.abs(s)) > 2**-20))
        self.assertCountEqual(s, [0, 0, 0])
        self.assertCountEqual(r, [np.inf, np.inf, np.inf])

    def test_leading_zero(self):
        p = p_leading_zero(1)
        s, r = ea.roots(p, radius=True)
        self.assertCountEqual(s, [np.inf])
        self.assertCountEqual(r, [0])

    def test_leading_small(self):
        p = p_leading_small(2**-50)
        with warnings.catch_warnings(record=True) as Lw:
            s, r = ea.roots(p, radius=True)
            self.assertNoWarning(Lw)
        n = np.count_nonzero(r/np.abs(s) > 2**-20)
        self.assertEqual(n, 0)
        p = p_leading_small(2**-100)
        with warnings.catch_warnings(record=True) as Lw:
            s, r = ea.roots(p, radius=True)
            self.assertNoWarning(Lw)
        n = np.count_nonzero(r/np.abs(s) > 2**-20)
        self.assertEqual(n, 0)

    def test_empty(self):
        s, r = ea.roots([], radius=True)
        self.assertEqual(s.size, 0)
        self.assertEqual(r.size, 0)
    
    def test_one(self):
        s, r = ea.roots([1], radius=True)
        self.assertCountEqual(s, [])
        self.assertCountEqual(r, [])

    def test_zero(self):
        s, r = ea.roots([0], radius=True)
        self.assertCountEqual(s, [])
        self.assertCountEqual(r, [])

    def test_one_zero(self):
        s, r = ea.roots([1, 0], radius=True)
        self.assertCountEqual(s, [np.inf])
        self.assertCountEqual(r, [0])

    def test_zero_zero(self):
        with warnings.catch_warnings(record=True) as Lw:
            s, r = ea.roots([0, 0], radius=True)
            self.assertWarningTolerance(Lw)
        self.assertCountEqual(s, [0])
        self.assertCountEqual(r, [np.inf])


    def test_underflow_1(self):
        p = p_small_coeffs(1, 2**-1020)
        with warnings.catch_warnings(record=True) as Lw:
            s, r = ea.roots(p, radius=True)
            self.assertWarningTolerance(Lw)
        self.assertLess(r[0], 2**-20)

    def test_underflow_2(self):
        p = p_small_coeffs(2, 2**-1020)
        with warnings.catch_warnings(record=True) as Lw:
            s, r = ea.roots(p, radius=True)
            self.assertWarningTolerance(Lw)
        self.assertLess(r[0], 2**-20)
        self.assertLess(r[1], 2**-20)

    def test_underflow_4(self):
        p = p_small_coeffs(4, 2**-1022)
        with warnings.catch_warnings(record=True) as Lw:
            s, r = ea.roots(p, radius=True)
            self.assertEqual(len(Lw), 1)
            self.assertTrue(issubclass(Lw[-1].category, RuntimeWarning))
            self.assertIn("overflow", str(Lw[-1].message))

    def test_multiple_polynomials(self):
        p = np.zeros((3, 27))
        p[0, :11] = p_truncated_exponential(10)
        p[1, :20] = p_truncated_exponential(19)
        p[2, :]   = p_truncated_exponential(26)
        s, r = ea.roots(p, radius=True)
        n = np.count_nonzero(s == np.inf)
        self.assertEqual(n, 23)

    def test_random_real_50_perturbation(self):
        p = p_random_normal_real(50)
        r = 2**-15*np.abs(p_random_normal_real(50))
        with warnings.catch_warnings(record=True) as Lw:
            self.assertSameRoots(p, "random real", 50, in_radius=r)
            self.assertWarningTolerance(Lw)

    def test_random_real_100_perturbation(self):
        p = p_random_normal_real(100)
        r = 2**-15*np.abs(p_random_normal_real(100))
        with warnings.catch_warnings(record=True) as Lw:
            self.assertSameRoots(p, "random real", 100, in_radius=r)
            self.assertWarningTolerance(Lw)

    def test_random_real_500_perturbation(self):
        p = p_random_normal_real(500)
        r = 2**-15*np.abs(p_random_normal_real(500))
        with warnings.catch_warnings(record=True) as Lw:
            self.assertSameRoots(p, "random real", 500, in_radius=r)
            self.assertWarningTolerance(Lw)

    def test_overflow_bad_pivot_estimation(self):
        np.random.seed(2)
        p = np.random.randint(-2**16, 2**16, 383)
        with warnings.catch_warnings(record=True) as Lw:
            self.assertSameRoots(p, "random polynomial", 383)
            self.assertNoWarning(Lw)

    def test_zero_real_parts(self):
        p = [1j, 1j]
        self.assertSameRoots(p, "zero real parts", 1)

    def test_deg256(self):
        p = [32, 1, 1, -7, 3, 1, 1, -29, -36, -1, 1, -127, -15, -1, 3, -4,
                1, 1, -1, -55, 1, 5, 65, -1, 57, 56, 3, -1, 22, -1, 92, 1,
                1, -1, 1, 1, 1, 6, -3, -180, -7, -30, 127, 1, 1, 5, 10, 3,
                1, 1, 1 , 1, 35, 20, 54, -18, -1, 1, 1, 1, 1, -19, -1, 10,
                122, 15, 1, 255, 249, -2, 1, 1, 3, -1, -15, -86, -2, -45,
                1, 5, 2, 31, 1, -2, 1, -1, 1, 3, -1, -1, 19, -1, 1, 1, 3,
                -30, -3, 1, 1, -110, 1, 1, -10, 41, 4, -135, -1, -51, -1,
                1, -1, -1, 32, 1, -2, 1, 8, 1, 1, 1, -102, 1, 127, 14, 1,
                120, -82, 1, -149, -8, 224, -208, 1, -93, 3, -197, 27, 1,
                -3, 85, -3, 1, 7, 1, 1, 5, 14, 5, -2, 1, 1, 1, 232, -4, -1,
                1, 1, 1, -215, 1, 97, 15, 1, -249, -2, 1, -1, -7, 2, -35,
                -1, 1, -1, 2, 29, -88, 1, 1, -4, -24, -48, -33, -1, 12, -1,
                -3, -1, 1, 1, -188, -14, -1, 3, -15, 33, 1, -1, -7, -5, 29,
                64, 1, -14, -24, 1, -3, -7, 42, -92, 1, 62, 1, -20, 2, 71,
                1, -1, 1, -3, 136, 1, 1, 4, 1, 1, 1, -196, 8, 7, 1, -16,
                -1, -32, 1, 195, -4, 45, 10, 1, -1, 1, 1, 1, 47, 2, -6, 1,
                -12, 1, 10, 1, 1, -1, 1, -1, 1, 256]
        q = [c/256 for c in p]
        self.assertSameRoots(p, "random real", 256)
        with warnings.catch_warnings(record=True) as Lw:
            self.assertSameRoots(q, "random real", 256)
            self.assertNoWarning(Lw)


    #def test_linear(self):
    #    p = p_linear(1)
    #    self.assertSameRoots(p, "x+1", 500)



if __name__ == '__main__':
    args = ['TestEARoots.test_' + s for s in sys.argv]
    unittest.main(argv=args)

