import ctypes
import numpy as np

c_earoots = ctypes.CDLL("libearoots.so")
#c_earoots = ctypes.CDLL("/usr/lib/python3.10/site-packages/earoots/_interface.cpython-310-x86_64-linux-gnu.so") 

c_earoots.ea_roots.argtypes = [
    ctypes.POINTER(ctypes.c_double),
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_double,
    ctypes.c_int,
    ctypes.POINTER(ctypes.c_double),
    ctypes.POINTER(ctypes.c_double),
]

#import line_profiler as lp
#profile = lp.LineProfiler()
#@profile
def ctroots(p):
    #lp = (ctypes.c_double * (2*len(p)))(*p.astype(complex).view(float))
    lp = p.astype(complex).view(float).ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    ls = (ctypes.c_double * (2*len(p)-2))()
    lr = (ctypes.c_double * (len(p)-1))()
    c_earoots.ea_roots(lp, p.size, 500, 2 ** (-25), 0, ls, lr)
    #return ls, lr
    #return list(ls), list(lr)
    #return [complex(r,i) for r,i in zip(*(iter(ls),)*2)]
    return [complex(r,i) for r,i in zip(*(iter(ls),)*2)], list(lr)
    #return [complex(r,i) for r,i in zip(ls[::2], ls[1::2])], list(lr)
    #return np.array(ls).view(complex), np.array(lr)

