import ctypes
import earoots as ea
import numpy as np


c_earoots = ctypes.CDLL("libearoots.so")
#c_earoots = ctypes.CDLL("/usr/lib/python3.10/site-packages/earoots/_interface.cpython-310-x86_64-linux-gnu.so") 

c_earoots.ea_roots.argtypes = [
    np.ctypeslib.ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_double,
    ctypes.c_int,
    np.ctypeslib.ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
    np.ctypeslib.ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
]


#import line_profiler
#profile = line_profiler.LineProfiler()
#@profile
def ctroots(p):
    a = p.view(float)
    b = np.empty(2*(p.size - 1), dtype=float)
    c = np.empty(b.size, dtype=float)
    c_earoots.ea_roots(a, p.size, 500, 2 ** (-25), 0, b, c)
    return b.view(complex), c


