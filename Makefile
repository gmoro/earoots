all: release

release:
	mkdir -p build
	cd build && cmake -DCMAKE_BUILD_TYPE=Release ../
	make -C build -j4 VERBOSE=1

install:
	CMAKE_NO_VERBOSE=1 make -C build install -j4 VERBOSE=1

debug:
	mkdir -p build
	cd build && cmake -DCMAKE_BUILD_TYPE=Debug ../
	make -C build -j4 VERBOSE=1

clean:
	rm -r build

