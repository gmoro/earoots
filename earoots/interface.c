// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "Python.h"
#include "numpy/ndarrayobject.h"
#include "earoots.h"

static PyObject* py_ea_roots(PyObject* self, PyObject* args)
{
    PyObject *arg1=NULL, *arg2=NULL, *arg3=NULL, *arg4=NULL;
    int max_iter, verbose;
    double max_error;
    PyArrayObject *coefficients, *in_radii, *values, *radii;
    if(!PyArg_ParseTuple(args, "OOOOidi", &arg1, &arg2, &arg3, &arg4, &max_iter, &max_error, &verbose)) {
        return NULL;
    }
    // Check input type
    if( !PyArray_Check(arg1) || !PyArray_Check(arg2) || !PyArray_Check(arg3) || !PyArray_Check(arg4)) {
        PyErr_SetString(PyExc_TypeError, "The arguments should be numpy arrays");
        return NULL;
    }

    coefficients = (PyArrayObject*) arg1;
    in_radii = (PyArrayObject*) arg2;
    values = (PyArrayObject*) arg3;
    radii = (PyArrayObject*) arg4;


    int Dc = PyArray_NDIM(coefficients);
    int Di = PyArray_NDIM(in_radii);
    int Dv = PyArray_NDIM(values);
    int Dr = PyArray_NDIM(radii);
    // Check non empty arrays
    if((Dc<1) || (Dv<1) || (Dr<1) || (Di<1) ) {
        PyErr_SetString(PyExc_TypeError, "Array should have a positive dimension");
        return NULL;
    }

    npy_intp Nc = PyArray_DIM(coefficients, Dc-1);
    npy_intp Ni = PyArray_DIM(in_radii, Di-1);
    npy_intp Nv = PyArray_DIM(values, Dv-1);
    npy_intp Nr = PyArray_DIM(radii, Dr-1);

    // Check layout
    if(!PyArray_ISCARRAY_RO(coefficients)) {
        PyErr_SetString(PyExc_TypeError, "First array should have a C contiguous layout");
        return NULL;
    }
    if(!PyArray_ISCARRAY_RO(in_radii)) {
        PyErr_SetString(PyExc_TypeError, "Second array should have a C contiguous layout");
        return NULL;
    }
    if(!PyArray_ISCARRAY(values)) {
        PyErr_SetString(PyExc_TypeError, "Third array should have a C contiguous layout");
        return NULL;
    }
    if(!PyArray_ISCARRAY(radii)) {
        PyErr_SetString(PyExc_TypeError, "Fourth array should have a C contiguous layout");
        return NULL;
    }
    // Check sizes
    if((Dc != Di) || (Dc != Dv) || (Dc != Dr) || (Nc > 0 && Nv != Nc-2) || (Nc == 0 && Nv != 0) || (Nv != 2*Nr) || (2*Ni != Nc)) {
        PyErr_SetString(PyExc_TypeError, "Array sizes don't match");
        return NULL;
    }
    npy_intp Pc = 1;
    for(int i=0; i < Dc-1; i++) {
        Pc *= PyArray_DIM(coefficients, i);
    }
    npy_intp Pi = 1;
    for(int i=0; i < Di-1; i++) {
        Pi *= PyArray_DIM(coefficients, i);
    }
    npy_intp Pv = 1;
    for(int i=0; i < Dv-1; i++) {
        Pv *= PyArray_DIM(values, i);
    }
    npy_intp Pr = 1;
    for(int i=0; i < Dr-1; i++) {
        Pr *= PyArray_DIM(radii, i);
    }
    if((Pi != Pc) || (Pr != Pc) || (Pv != Pc)) {
        PyErr_SetString(PyExc_TypeError, "Input shapes don't match");
        return NULL;
    }
    // Check no alias
    if( PyArray_DATA(coefficients) == PyArray_DATA(radii) ) {
        PyErr_SetString(PyExc_TypeError, "Output must be different from coefficients");
        return NULL;
    }

    // Computation
    int total = 0;
    int guaranteed = 1;
    if(PyArray_DESCR(coefficients)->type_num == NPY_FLOAT64 &&
       PyArray_DESCR(in_radii)->type_num == NPY_FLOAT64 &&
       PyArray_DESCR(values)->type_num == NPY_FLOAT64 &&
       PyArray_DESCR(radii)->type_num == NPY_FLOAT64) {
        for(npy_intp i=0; i < Pc; i++) {
            int status;
            status = ea_roots((double*) PyArray_DATA(coefficients) + i*Nc,
                              (double*) PyArray_DATA(in_radii) + i*Ni,
                              Ni,
                              max_iter, max_error, verbose,
                              (double*) PyArray_DATA(values) + i*Nv,
                              (double*) PyArray_DATA(radii) + i*Nr);
            total += abs(status);
            guaranteed = (status < 0)? 0 : guaranteed;
        }
    } else {
        PyErr_SetString(PyExc_TypeError, "Array type should be complex128, complex128 and float64");
        return NULL;
    }
    if(!guaranteed) {
        total = -total;
    }
    return PyLong_FromLong(total);
}

static PyMethodDef EARootsMethods[] = {
    {"_c_ea_roots", py_ea_roots, METH_VARARGS,
     "Solve a polynomial using Ehrlich-Aberth method."},
    {NULL, NULL, 0, NULL}
};

#if PY_VERSION_HEX >= 0x03000000
static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "_interface",
    NULL,
    -1,
    EARootsMethods,
    NULL,
    NULL,
    NULL,
    NULL
};

PyMODINIT_FUNC PyInit__interface(void)
{
    PyObject *m;
    m = PyModule_Create(&moduledef);
    if (m == NULL) {
        return NULL;
    }
    import_array();
    return m;
}
#else
PyMODINIT_FUNC init_interface(void)
{
    PyObject *m;
    m = Py_InitModule("_interface", EARootsMethods);
    if (m == NULL) {
        return;
    }
    import_array();
}
#endif
