# Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

from earoots._interface import _c_ea_roots
import numpy as _np
import sys as _sys
import warnings as _warnings

def roots(p, in_rads=None, max_iter=50, rtol=2**(-25), radius=False, verbose=0, out_sols=None, out_rads=None):
    """ Computes the complex roots of polynomials
        
        This function uses the Ehrlich-Aberth to compute the complex roots
        of a polynomial. It uses the Newton polygon for the initial points.

        Output:
            An array containing the solutions of the input polynomials, and
            if `radius` is True, a second array containing bounds on the distance to
            closest root.

            The shapes of the output arrays match the shape of the input
            polynomials array, where the size of last axis has been
            decreased by one.

            If the input polynomial containing leading zeros, the output
            array will be padded with solutions centered at infinity, with
            disk radius set to zero.

        
        Parameters:
            p (array): an array of complex number representing a polynomial
                in the dense representation, where the number of index `k`
                represents the coefficient of the monomial of degree `k`. If
                the the array has multiple dimensions, it represents several
                polynomial, each stored in the last axis.

            max_iter (integer): maximal number of Ehrlich-Aberth
                iterations.
            
            rtol (float): maximal distance between the returned
                solution and the exact root, divided by the absolute value
                of the root.
            
            radius (boolean): if True, returns the radius of enclosing
                disks. The union of the disks contains all the roots, and each
                disk contains at least one root.
            
            verbose (int): progress messages on stderr; 0 no messages, 1
                the number of iterations, 2 the iterations and intermediate
                values.
            
            out_sols (array): array for storing the approximated roots;
                the size of its last axis should match the degree of the
                input polynomials.
            
            out_rads (array): array for storing the radii of the disks if
                `radius` is True; its size of its last axis should match
                the degree of the input polynomials.
    """
    if(not isinstance(p, _np.ndarray) or p.ndim<1):
        p = _np.array(p, dtype=_np.complex128, ndmin=1)
    elif(p.dtype != _np.complex128 or not p.flags['C_CONTIGUOUS']):
        p = p.astype(_np.complex128)
    sol_shape = p.shape[:-1] + (max(0, p.shape[-1]-1),)
    if out_sols is None:
        out_sols = _np.empty(sol_shape, dtype=_np.complex128)
    if out_rads is None:
        out_rads = _np.empty(sol_shape, dtype=_np.float64) 
    if in_rads is None:
        in_rads = _np.zeros(p.shape, dtype=_np.float64) 
    elif(not isinstance(in_rads, _np.ndarray) or in_rads.ndim<1):
        in_rads = _np.array(in_rads, dtype=_np.float64, ndmin=1)
    elif(in_rads.dtype != _np.float64 or not in_rads.flags['C_CONTIGUOUS']):
        in_rads = in_rads.astype(_np.float64)
    
    status = _c_ea_roots(p.view(_np.float64), in_rads, out_sols.view(_np.float64), out_rads, max_iter, rtol, verbose)

    zero = (out_sols == 0)
    if radius == True and status < 0:
        _warnings.warn("overflow encountered, radius might be wrong", RuntimeWarning, stacklevel=2)
    elif any(out_rads[zero] > rtol) or any(out_rads[~zero]/abs(out_sols[~zero]) > rtol):
        _warnings.warn("required tolerance not reached for all roots", RuntimeWarning, stacklevel=2)
    if verbose >= 1:
        print(f"{abs(status)} iterations", file=_sys.stderr)
    if radius:
        return out_sols, out_rads
    else:
        return out_sols



