`EARoots` is a root-finding software written in C. It uses Ehrlich-Aberth
method to find polynomial complex roots in double precision. The code has
been written to benefit from auto-vectorization.

# Installation

## Standalone

The standalone library can be installed with:

    make
    sudo make install

## Python

The package `earoots` is a Python 3 interface to `EARoots` and can be installed with:

    pip install git+https://gitlab.inria.fr/gmoro/earoots.git

or after cloning this repository with:

    sudo python setup.py install


# Source organisation

`src` contains the code written in C. The main function is `ea_roots`.

`earoots` contains the python wrapper. The main function is `roots`. 

`tests` contains the test suite based on python.

`examples` contains simple example of use in C and in python.

# Licence

EARoots is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version. See <http://www.gnu.org/licenses/>.


