// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#ifndef EAROOTS_H
#define EAROOTS_H

#ifdef __cpluscplus
extern "C" {
#endif

// Computes disks enclosing the roots of the polynomial. Returns 0 if no overflow,
// nor underflow occurred, and 1 otherwise.
int ea_roots(const double* p, const double* in_rads, int n, int max_iter, double max_error, int verbose,
             double* z, double* out_rads);

// Evaluates a polynomial with ball coefficients with absolute radius on a vector of balls with a bound on the maximal relative radius
void ea_evaluate(const double * coeffs, const double * abs_crads, int clen,   // input poly: coeffs has size 2*clen, in_crads has size clen
                 const double * points, int plen, const double max_rel_prads, // input points: points has size 2*plen,
                 double * z, double * abs_zrads );                            // output value: z should have size 2*plen, and abs_zrads size plen

#ifdef __cpluscplus
}
#endif
#endif
