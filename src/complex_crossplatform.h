// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#ifndef COMPLEX_CROSSPLATFORM_H
#define COMPLEX_CROSSPLATFORM_H

#include <complex.h>

#if _MSC_VER
typedef _Dcomplex dcomplex;
static dcomplex make_dcomplex(double r, double i) {
    dcomplex res = {r, i};
    return res;
}
#else
typedef double complex dcomplex;
static dcomplex make_dcomplex(double r, double i) {
    return r + i*I;
}
#endif

static inline dcomplex cneg(dcomplex a) {
    double r = -creal(a);
    double i = -cimag(a);
    dcomplex res = make_dcomplex(r, i);
    return res;
}

static inline dcomplex cadd(dcomplex a, dcomplex b) {
    double r = creal(a) + creal(b);
    double i = cimag(a) + cimag(b);
    dcomplex res = make_dcomplex(r, i);
    return res;
}
    
static inline dcomplex csub(dcomplex a, dcomplex b) {
    double r = creal(a) - creal(b);
    double i = cimag(a) - cimag(b);
    dcomplex res = make_dcomplex(r, i);
    return res;
}

static inline dcomplex cmul(dcomplex a, dcomplex b) {
    double r = creal(a)*creal(b) - cimag(a)*cimag(b);
    double i = creal(a)*cimag(b) + cimag(a)*creal(b);
    dcomplex res = make_dcomplex(r, i);
    return res;
}

static inline dcomplex cdiv(dcomplex a, dcomplex b) {
    double m = creal(b)*creal(b) + cimag(b)*cimag(b);
    double r = (creal(a)*creal(b) + cimag(a)*cimag(b))/m;
    double i = (creal(b)*cimag(a) - creal(a)*cimag(b))/m;
    dcomplex res = make_dcomplex(r, i);
    return res;
}

#endif

