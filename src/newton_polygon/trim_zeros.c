#include <math.h>

int ea_trim_zeros(const double* p, const double* in_rads, int n, double* z, double* rad, int* fz, int* bz)
{
    int i;
    double bc = INFINITY,
           br = 0.0;

    for(i=0; i<n-1 && p[2*i] == 0.0 && p[2*i+1] == 0.0 && in_rads[i] == 0.0; i++) {
        z[2*i] = 0.0;
        z[2*i+1] = 0.0;
        rad[i] = 0.0;
    }
    *fz = i;

    if(*fz == n-1 && p[2*(n-1)] == 0.0 && p[2*(n-1)+1] == 0.0 && in_rads[n-1] == 0.0) {
        bc = 0;
        br = INFINITY;
        *fz = 1;
    }

    for(i=0; i<n-1 && p[2*(n-1-i)] == 0.0 && p[2*(n-1-i)+1] == 0 && in_rads[n-1-i] == 0.0; i++) {
        z[2*(n-2-i)] = bc;
        z[2*(n-2-i)+1] = 0.0;
        rad[n-2-i] = br;
    }
    *bz = i;
    return n-*fz-*bz;
}

