// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <math.h>

static int is_counter_clockwise(double a, double b, double x, double y, double u, double v)
{
    double det = v*(x-a) + y*(a-u) + b*(u-x);
    return (det>0);
}

int ea_lower_convex_hull(const double* px, const double* py, int n, int* result)
{
    if(n<1) {
        return 0;
    } else {
        result[0] = 0;
        int k=1;
        for (int i=1; i<n; i++) {
            if(isfinite(py[i])) {
                while(k>1 && !is_counter_clockwise(px[result[k-2]], py[result[k-2]],
                                                   px[result[k-1]], py[result[k-1]],
                                                   px[i], py[i])) {
                    k--;
                }
                result[k] = i;
                k++;
            }
        }
        return k;
    }
}

