// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include "newton_polygon.h"
#include <math.h>

int ea_newton_polygon(const double* p_r, const double* p_i, const double* ep, int n, double* tmp_x, double* tmp_y,
                      int* np)
{
    for(int i=0; i<n; i++) {
        tmp_x[i] = i;
        //tmp_y[i] = -creal(clog(p_r[i] + I*p_i[i]));
        tmp_y[i] = -log(hypot(p_r[i],p_i[i])+ldexp(ep[i], -53));
    }
    int m = ea_lower_convex_hull(tmp_x, tmp_y, n, np);
    return m;
}
