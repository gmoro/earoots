// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <math.h>
#include <fenv.h>
#include <stdio.h>

double ea_get_pivot(const double* ap, int n, const int* np, int m)
{
    if(m<2) {
        return INFINITY;
    }
    //return 1;
    /* guess to avoid unnecessary pivot */
    int i;
    i = np[m-2];
    double d = n-1;
    double delta = d-i;
    // Bound on the evaluation of p and Durand Kerner at the Fujiwara's root bound
    // The distance between two roots is at most twice the Fujiwara's root bound
    double z = ap[n-1]*ldexp(pow(ap[i]/ap[n-1], d/delta), 3*(n-1));
    if(isfinite(z*z)) {
        return INFINITY;
    } else {
        return 1;
    }
}

