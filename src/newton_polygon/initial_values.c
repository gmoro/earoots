// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <math.h>
#include "complex_crossplatform.h"

void ea_initial_values(const double* p_r, const double* p_i,
                       const int* np, int m, double* z_r, double* z_i)
{
    int p = 0;                         
    double pi = acos(-1);
    for(int i=1; i<m; i++) {
        int q = np[i];
        if( (q-p == 1) && (i < m-1) && (np[i+1]-q == 1) ) {
            i++;
            int r = np[i];
            //dcomplex a = p_r[r] + dI*p_i[r];
            dcomplex a = make_dcomplex(p_r[r], p_i[r]);
            dcomplex bh = make_dcomplex(p_r[q]/2, p_i[q]/2);
            dcomplex c = make_dcomplex(p_r[p], p_i[p]);
            //dcomplex z0 = (-b + csqrt(b*b-4*a*c))/(2*a);
            //dcomplex z1 = (-b - csqrt(b*b-4*a*c))/(2*a);
            dcomplex z0 = cdiv(cadd(cneg(bh), csqrt(csub(cmul(bh, bh), cmul(a, c)))),a);
            dcomplex z1 = cdiv(csub(cneg(bh), csqrt(csub(cmul(bh, bh), cmul(a, c)))),a);
            if(creal(z0) == creal(z1)) {
                dcomplex one = make_dcomplex(1, 0);
                dcomplex epsilon = make_dcomplex(ldexp(1,-50), 0);
                z0 = cmul(z0, csub(one, epsilon));
                z1 = cmul(z1, cadd(one, epsilon));
            }
            z_r[p] = creal(z0);
            z_i[p] = cimag(z0);
            z_r[p+1] = creal(z1);
            z_i[p+1] = cimag(z1);
            p = r;
        } else {
            double d = q-p;
            dcomplex invd = make_dcomplex(1/d, 0);
            dcomplex cp = make_dcomplex(p_r[p], p_i[p]);
            dcomplex cq = make_dcomplex(p_r[q], p_i[q]);
            dcomplex c = cpow(cneg(cdiv(cp,cq)), invd);
            if(creal(c) == 0.0 && cimag(c) == 0.0) {
                c = make_dcomplex(ldexp(1, -500/(q-p)), 0);
            }
            for(int j=0; j<q-p; j++) {
                dcomplex o = make_dcomplex(0, 2*pi*j/d);
                dcomplex r = cmul(c, cexp(o));
                z_r[p+j] = creal(r);
                z_i[p+j] = cimag(r);
            }
            p = q;
        }
    }
}
