// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#ifndef ERROR_BOUND_H
#define ERROR_BOUND_H

// Bound the error on the value of Horner evaluation
void ea_horner_abs_with_error_bound(const double* p_r, const double* p_i, int n, double* aep_u,
                                    const double* z_r, const double* z_i, int m, double e_z_ulp,
                                    double* results_r, double* results_i, double* results_abs, double* error);

// Durand-Kerner coefficient to bound the error on the roots
void ea_durand_kerner_abs_coefficients(double pr, double pi,
                                       const double* restrict in_r, const double* restrict in_i, int m,
                                       int n_piv, double* out);
void ea_durand_kerner_reciprocal_abs_coefficients(double pr, double pi,
                                                  const double* restrict in_r, const double* restrict in_i, int m,
                                                  int n_piv, double* d, double* out);

// Bound the error on the absolute value of Durand-Kerner's weight denominator
void ea_durand_kerner_abs_error_bound(const double* adk, int m, int n_piv, double* results);

// Bound the error on the union of the roots or the roots
void ea_roots_error_bound(const double* avp, const double* evp,
                          const double* adk, const double* edk, int m,
                          int d, double* results);

// Bound on the absolute error
void ea_derivative_error_bound(const double* c_r, const double* c_i, double* in_ae, int n, double* out_ae);

#endif
