// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include "polynomial.h"
#include "error_bound.h"
#include "vector.h"
#include "ehrlich_aberth.h"
#include "newton_polygon.h"

#include <stdlib.h>
#include <math.h>
#include <fenv.h>

enum mem_indices {p_r, p_i, ep, z_r, z_i, r, ap, dp_r, dp_i, edp, adp, Rp_r, Rp_i, eRp,
                  aRp, zRdp_r, zRdp_i, ezRdp, azRdp, vp_r, vp_i, evp, vdp_r,
                  vdp_i, evdp, iz_r, iz_i, az, iaz, bs_r, bs_i, d0, d1,
                  avp, avdp, adk, edk, total};

static void init_polynomials(int n, double v[total][n])
{
    ea_vector_abs(v[p_r], v[p_i], n, v[ap]);
    ea_derivative(v[p_r], v[p_i], n, v[dp_r], v[dp_i]);
    ea_vector_abs(v[dp_r], v[dp_i], n-1, v[adp]);
    ea_derivative_error_bound(v[p_r], v[p_i], v[ep], n, v[edp]);
    ea_d_reciprocal(v[ep], n, v[eRp]);
    ea_dd_reciprocal(v[p_r], v[p_i], n, v[Rp_r], v[Rp_i]);
    ea_vector_abs(v[Rp_r], v[Rp_i], n, v[aRp]);
    ea_dd_reciprocal(v[dp_r], v[dp_i], n-1, v[zRdp_r], v[zRdp_i]);
    ea_dd_mul_z(v[zRdp_r], v[zRdp_i], n-1, v[zRdp_r], v[zRdp_i]);
    ea_vector_abs(v[zRdp_r], v[zRdp_i], n, v[azRdp]);
    ea_d_reciprocal(v[edp], n-1, v[ezRdp]);
    ea_d_mul_z(v[ezRdp], n-1, v[ezRdp]);
}

static double init_values(int n, double v[total][n], int np[n])
{
    double pivot;
    int n_np = ea_newton_polygon(v[p_r], v[p_i], v[ep], n, v[d0], v[d1], np);
    ea_initial_values(v[p_r], v[p_i], np, n_np, v[z_r], v[z_i]);
    pivot = ea_get_pivot(v[ap], n, np, n_np);
    return pivot;
}

static int solve_pivot(int n, double v[total][n], double pivot, int max_iter, double max_error, int verbose)
{                           
    int radius_check_period = 5;
    int m = n-1;
    int n_piv = m;
    int status;
    int i;
    
    // iteration loop with max_iter+1 iterations,
    // including max_iter updates of z
    for(i=0; i<max_iter+1; i++) {
        // sort and inverse big values to reduce overflow
        ea_vector_abs(v[z_r], v[z_i], m, v[az]);
        n_piv = ea_vector_partition_pivot(pivot, v[z_r], v[z_i], v[az], m);
        // error on inverse for (1/(z zbar))*zbar is 4 ulp
        ea_vector_inverse(v[z_r] + n_piv, v[z_i] + n_piv, m-n_piv, v[iz_r], v[iz_i]);
        ea_vector_abs(v[iz_r], v[iz_i], m-n_piv, v[iaz]);

        // check radius for early termination
        if(((i - max_iter) % radius_check_period) == 0) {
            feclearexcept(FE_OVERFLOW | FE_INVALID);

            ea_horner_abs_with_error_bound(v[p_r], v[p_i], n, v[ep], v[z_r], v[z_i], n_piv, 0, v[vp_r], v[vp_i], v[avp], v[evp]);
            ea_horner_abs_with_error_bound(v[Rp_r], v[Rp_i], n, v[eRp], v[iz_r], v[iz_i], m-n_piv, 4, v[vp_r]+n_piv, v[vp_i]+n_piv, v[avp]+n_piv, v[evp]+n_piv);

            // compute the radius with Durand-Kerner weights and Gerschgorin bound
            ea_durand_kerner_abs_coefficients(v[p_r][n-1], v[p_i][n-1], v[z_r], v[z_i], m, n_piv, v[adk]);
            ea_durand_kerner_reciprocal_abs_coefficients(v[p_r][n-1], v[p_i][n-1], v[z_r], v[z_i], m, n_piv, v[d0], v[adk]);
            ea_durand_kerner_abs_error_bound(v[adk], m, n_piv, v[edk]);

            // Ensure that the union of the disks contain all the roots
            ea_roots_error_bound(v[avp], v[evp], v[adk], v[edk], m, n-1, v[r]);

            // Verbose
            ea_vector_print(stderr, verbose, "variables: z_r, z_i, vp_r, vp_i, avp, evp, adk, edk",
                            i, m, 8, v[z_r], v[z_i], v[vp_r], v[vp_i], v[avp], v[evp], v[adk], v[edk]);
            if((ea_vector_max(v[r], m) < max_error) || (i==max_iter)) {
                break;
            }
        } else {
        // compute the terms of the Newton correction factor without error
            ea_dc_horner(v[p_r], v[p_i], n, v[z_r], v[z_i], n_piv, v[vp_r], v[vp_i]);
            ea_dc_horner(v[Rp_r], v[Rp_i], n, v[iz_r], v[iz_i], m-n_piv, v[vp_r]+n_piv, v[vp_i]+n_piv);
        }
        // compute with reciprocal polynomials for large values
        ea_dc_horner(v[dp_r], v[dp_i], n-1, v[z_r], v[z_i], n_piv, v[vdp_r], v[vdp_i]);
        ea_dc_horner(v[zRdp_r], v[zRdp_i], n, v[iz_r], v[iz_i], m-n_piv, v[vdp_r]+n_piv, v[vdp_i]+n_piv);

        // compute the next z with Aberth-Ehrlich correction
        ea_borsch_supan_coefficients(v[z_r], v[z_i], m, v[bs_r], v[bs_i]);
        ea_ehrlich_aberth_update(v[vp_r], v[vp_i], v[vdp_r], v[vdp_i], v[bs_r], v[bs_i], m, v[z_r], v[z_i]);
    }
    // Ensure that each disk contains at least one root
    ea_horner_abs_with_error_bound(v[dp_r], v[dp_i], n-1, v[edp], v[z_r], v[z_i], n_piv, 0, v[vdp_r], v[vdp_i], v[avdp], v[evdp]);
    ea_horner_abs_with_error_bound(v[zRdp_r], v[zRdp_i], n, v[ezRdp], v[iz_r], v[iz_i], m-n_piv, 4, v[vdp_r]+n_piv, v[vdp_i]+n_piv, v[avdp] + n_piv, v[evdp]+n_piv);
    ea_roots_error_bound(v[avp], v[evp], v[avdp], v[evdp], m, n-1, v[d0]);

    // Verbose
    ea_vector_print(stderr, verbose, "variables: z_r, z_i, vdp_r, vdp_i, avdp, evdp, avp, evp",
                    i, m, 8, v[z_r], v[z_i], v[vdp_r], v[vdp_i], v[avdp], v[evdp], v[avp], v[evp]);
    
    ea_vector_max2(v[d0], v[r], m, v[r]);

    status = fetestexcept(FE_OVERFLOW | FE_INVALID)? -1-i : i+1;
    return status;
}
                    
/* p is an array of size 2n representing the complex coefficients of a
   polynomial. The even indices are the real parts, and the odd indices are
   the imaginary part.

   z are the complex root approximations, layout in the same way.
 */
int ea_roots(const double* p, const double* in_rads, int n, int max_iter, double max_error, int verbose,
             double* z, double* out_rads)
{
    if(n<=1) {
        return 0;
    }
    int nt, fz, bz, status;
    nt = ea_trim_zeros(p, in_rads, n, z, out_rads, &fz, &bz);
    double (*v)[nt] = malloc(total*nt*sizeof(double));
    int* np = malloc(nt*sizeof(int));
    for(int i=0; i<nt; i++) {
        v[p_r][i] = p[2*(i+fz)];
        v[p_i][i] = p[2*(i+fz)+1];
        v[ep][i] = ldexp(in_rads[i], 53);
    }
    init_polynomials(nt, v);
    double pivot = init_values(nt, v, np);
    status = solve_pivot(nt, v, pivot, max_iter, max_error, verbose);
    for(int i=0; i<nt-1; i++) {
        z[2*(i+fz)] = v[z_r][i];
        z[2*(i+fz)+1] = v[z_i][i];
        out_rads[i+fz] = v[r][i];
    }
    free(np);
    free(v);
    return status;
}

void ea_evaluate(const double * coeffs, const double * abs_crads, int clen,   // input poly: coeffs has size 2*clen, in_crads has size clen
                 const double * points, int plen, const double max_rel_prads, // input points: points has size 2*plen,
                 double * z, double * abs_zrads )                             // output value: z should have size 2*plen
{
    double *cre, *cim, *cra, *pre, *pim, *vre, *vim, *vra, *vabs, *v;
    double pra;
    v = malloc((clen*3+plen*6)*sizeof(double));

    cre = v; cim = cre+clen; cra = cim+clen;
    pre = cra+clen; pim = pre+plen;
    vre = pim+plen; vim = vre+plen; vabs = vim+plen; vra = vabs+plen;
    for(int i=0; i<clen; i++) {
        cre[i] = coeffs[2*i];
        cim[i] = coeffs[2*i+1];
        cra[i] = ldexp(abs_crads[i], 53);
    }
    for(int i=0; i<plen; i++) {
        pre[i] = points[2*i];
        pim[i] = points[2*i+1];
    }
    pra = ldexp(max_rel_prads, 53);
    ea_horner_abs_with_error_bound(cre, cim, clen, cra, pre, pim, plen, pra, vre, vim, vabs, vra);

    for(int i=0; i<plen; i++) {
        z[2*i]   = vre[i];
        z[2*i+1] = vim[i];
        abs_zrads[i] = vra[i];
    }

    free(v);
}



