// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <math.h>

void ea_durand_kerner_abs_coefficients(double pr, double pi,
                                       const double* restrict in_r, const double* restrict in_i, int m,
                                       int n_piv, double* out)
{
    for(int i=0; i<n_piv; i++) {
        out[i] = 1;
    }
    // square part with the diagonal
    for(int i=1; i<n_piv; i++) {
        for(int j=0; j<n_piv-i; j++) {
            double dr, di, d2;
            dr = in_r[j] - in_r[i+j];
            di = in_i[j] - in_i[i+j];
            d2 = dr*dr + di*di;
            out[j]   = out[j]*d2;
            out[i+j] = out[i+j]*d2;
        }
    }
    // remaining part in the rectangular case n_piv < m
    for(int i=n_piv; i<m; i++) {
        for(int j=0; j<n_piv; j++) {
            double dr, di, d2;
            dr = in_r[j] - in_r[i];
            di = in_i[j] - in_i[i];
            d2 = dr*dr + di*di;
            out[j] = out[j]*d2;
        }
    }
    for(int i=0; i<n_piv; i++) {
        out[i] = sqrt(out[i])*hypot(pr, pi);
    }
}

void ea_durand_kerner_reciprocal_abs_coefficients(double pr, double pi,
                                                  const double* restrict in_r, const double* restrict in_i, int m, int n_piv,
                                                  double* d, double* out)
{
    for(int i=n_piv; i<m; i++) {
        d[i] = 1/(in_r[i]*in_r[i] + in_i[i]*in_i[i]);
        out[i] = 1/(in_r[i]*in_r[i] + in_i[i]*in_i[i]);
    }           
    // square part with the diagonal
    for(int i=1; i<m-n_piv; i++) {
        for(int j=n_piv; j<m-i; j++) {
            double dr, di, d2;
            dr = in_r[j] - in_r[i+j];
            di = in_i[j] - in_i[i+j];
            d2 = dr*dr + di*di;
            out[j]   = out[j]*d2*d[j];
            out[i+j] = out[i+j]*d2*d[i+j];
        }
    }
    // remaining part in the rectangular case n_piv < m
    for(int i=0; i<n_piv; i++) {
        for(int j=n_piv; j<m; j++) {
            double dr, di, d2;
            dr = in_r[j] - in_r[i];
            di = in_i[j] - in_i[i];
            d2 = dr*dr + di*di;
            out[j] = out[j]*d2*d[j];
        }
    }
    for(int i=n_piv; i<m; i++) {
        out[i] = sqrt(out[i])*hypot(pr, pi);
    }
}
