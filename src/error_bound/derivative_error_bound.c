#include <math.h>

void ea_derivative_error_bound(const double* c_r, const double* c_i, double* in_ae, int n, double* out_ae)
{
    double u = ldexp(1,-53);
    for(int i=0; i<n-1; i++) {
        out_ae[i] = (i+1)*(hypot(c_r[i+1], c_i[i+1]) + in_ae[i])/(1-(6+1)*u);
    }
}

