// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include "error_bound.h"
#include <math.h>

// simd parameters
//   Rbits size of registers
//   Nr number of registers
#ifdef __AVX512CD__
#define Rbits 512
#define Nr 32
#elif __AVX__
#define Rbits 256
#define Nr 16
#elif __SSE__
#define Rbits 128
#define Nr 16
#else
#define Rbits 64
#define Nr 16
#endif
#define Rbytes (Rbits/8)
#define Nd (Rbytes/8)
#define DBlock (Nd*Nr/2)
#define CSBlock (Nd*Nr/2)
#define CDBlock (Nd*Nr/4)

// This function provides a smaller bound on the error of the absolute
// value of horner evaluation of p on z
// The value represented by the input are in the balls p +- |p|e_p and
// z +- |z|e_z
// Algorithm derived from Algorithm 6.4 of
// Handbook of Floating-Point Arithmetic, Muller
// Error bounds based on Lemma 2.3 in 
// Verified solutions of linear systems without directed rounding
// Ogita, Rump, Oishi
void ea_horner_abs_with_error_bound(const double* p_r, const double* p_i, int n, double* aep_u,
                                const double* z_r, const double* z_i, int m, double e_z_u,
                                double* results_r, double* results_i, double* results_abs, double* error)
{
    double eu = ldexp(1,-53);
    double umin = ldexp(1,-1074);
    double delta = 1 + sqrt(5) + e_z_u; // 3 u
    if(n==1){
        for(int i=0; i<m; i++) {
            results_r[i] = p_r[0];
            results_i[i] = p_i[0];
            results_abs[i] = hypot(p_r[0], p_i[0]);
            double b = aep_u[0] + 4*results_abs[0];
            int ne = 4;
            error[i] = b*eu/(1-(ne+1)*eu); 
        }
    } else {
        for(int i=0; i < m; i+=CDBlock) {
            double x[CDBlock] = {0};
            double y[CDBlock] = {0};
            double u[CDBlock] = {0};
            double v[CDBlock] = {0};
            double w[CDBlock] = {0};
            double e[CDBlock] = {0};
            int p = (i+CDBlock<=m) ? CDBlock : (m%CDBlock);
            for(int k=0; k<p; k++) {
                u[k] = z_r[i+k];
                v[k] = z_i[i+k];
                w[k] = hypot(u[k], v[k]);                  // 4 + e_z u
                x[k] = p_r[n-1];
                y[k] = p_i[n-1];
                e[k] = (hypot(p_r[n-1], p_i[n-1]) + aep_u[n-1])/delta; // (4 + 2) + 3 + 1  = 10 u
            }
            for(int j=1; j<n-1; j++) {
                double a = p_r[n-1-j];
                double b = p_i[n-1-j];
                double c = aep_u[n-1-j] / delta + 5*umin; // 0 + 3 + 1 + 1 u
                for(int k=0; k<CDBlock; k++) {
                    #pragma STDC FP_CONTRACT ON
                    double s,t;
                    s = a + u[k]*x[k] - v[k]*y[k];
                    t = b + u[k]*y[k] + v[k]*x[k];
                    x[k] = s;
                    y[k] = t;
                    e[k] = c + fabs(s) + fabs(t) + w[k]*e[k]; // max(ec, 0, 0, ew + e_k + 1) + 3  = ew + e_k + 4  = e_k + 8 + e_z u
                }
            }
            for(int k=0; k<p; k++) {
                double s, t, b, c, d;
                s = p_r[0] + u[k]*x[k] - v[k]*y[k];
                t = p_i[0] + u[k]*y[k] + v[k]*x[k];
                results_r[i+k] = s;
                results_i[i+k] = t;
                results_abs[i+k] = hypot(s, t);      // 4 u
                // last error corrections
                c = aep_u[0] + (4+delta)*umin; // 2 u
                d = sqrt(5) + e_z_u; // 2 u
                b = c + d*hypot(s, t) + delta*w[k]*e[k]; // max(ec, 5, 3 + ew + e_k + 2) + 2 = e_k + 11 + e_z ulp;
                int ne = 17 + (n-2)*(8 + ceil(e_z_u)); //10 + (n-3)*(8 + e_z) + 11 + e_z + 4 = 17 + (n-2)*(8+e_z) 
                error[i+k] = eu*b / (1 - eu*(ne+1));
            }
        }
    }
}

#undef CDBlock
#undef CSBlock
#undef DBlock
#undef Nd
#undef Rbytes
#undef Nr
#undef Rbits
