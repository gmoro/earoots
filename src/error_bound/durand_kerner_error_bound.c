// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <math.h>

void ea_durand_kerner_abs_error_bound(const double* adk, int m, int n_piv, double* results)
{   
    // Error bounds based on Lemma 2.3 in 
    //   Verified solutions of linear systems without directed rounding, 2005
    //   Ogita, Rump, Oishi
    // Error analysis :
    //   before pivot :
    //     5 u in each of the m-1 loop iterations, minus one for the first multiplication by one
    //     divide by 2 and add one with the last square root
    //     4+1 u for the hypotenuse and the last multiplication
    //     the total is 5 + 2.5 (m-1) u
    //   after pivot :
    //     3 u for the initial values
    //     9 u in each of the m-1 loop iterations
    //     divide by 2 and add 1 with the last square root
    //     4+1 u for the hypotenuse and the last multiplication
    //     the total is (3+9*(m-1))/2 + 1 + 5 u = 7.5+4.5*(m-1) u
    double eu;
    int ne;
    eu = ldexp(1,-53);
    ne = ceil(5 + 2.5*(m-1));
    for(int i=0; i<n_piv; i++) {
        results[i] = eu*adk[i] / (1 - eu*(ne+1));
    }
    ne = ceil(7.5 + 4.5*(m-1));
    for(int i=n_piv; i<m; i++) {
        results[i] = eu*adk[i] / (1 - eu*(ne+1));
    }
}
