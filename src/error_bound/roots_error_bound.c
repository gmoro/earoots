// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <math.h>

void ea_roots_error_bound(const double* avp, const double* evp,
                          const double* adk, const double* edk, int m,
                          int d, double* results)
{
    double u = ldexp(1, -53);
    double umin = ldexp(1, -1074);
    for(int i=0; i<m; i++) {
        if(adk[i] > edk[i]) {
            results[i] = d*((avp[i] + evp[i])/(adk[i] - edk[i]) + umin);
            results[i] = results[i]/(1-(4+1)*u);
        } else {
            results[i] = INFINITY;
        }
    }
}
