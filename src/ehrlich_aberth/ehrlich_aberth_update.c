// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include "complex_crossplatform.h"
#include <math.h>


void ea_ehrlich_aberth_update(const double* vp_r, const double* vp_i,
                              const double* vdp_r, const double* vdp_i,
                              const double* bs_r, const double* bs_i, int m,
                              double* z_r, double* z_i)
{
    double pi = acos(-1);
    for(int i=0; i<m; i++) {
        if(isfinite(bs_r[i]) && isfinite(bs_i[i]) && ((vdp_r[i]) != 0 || (vdp_i[i] != 0))) {
            dcomplex a, p, dp, bs;
            //a = (vp_r[i] + dI*vp_i[i])/(vdp_r[i] + dI*vdp_i[i] - (vp_r[i] + dI*vp_i[i])*(bs_r[i] + dI*bs_i[i]));
            p  = make_dcomplex(vp_r[i], vp_i[i]);
            dp = make_dcomplex(vdp_r[i], vdp_i[i]); 
            bs = make_dcomplex(bs_r[i], bs_i[i]);
            a = cdiv(p, csub(dp, cmul(p, bs)));
            z_r[i] -= creal(a);
            z_i[i] -= cimag(a);
        } else {
            dcomplex o = make_dcomplex(0, 2*pi*i/m);
            dcomplex a = cexp(o);
            if(z_r[i] != 0.0 || z_i[i] != 0.0) {
                z_r[i] *= 1 + ldexp(creal(a), -52 + 2*log(m));
                z_i[i] *= 1 + ldexp(cimag(a), -52 + 2*log(m));
            } else {
                z_r[i] = ldexp(creal(a), -500/m);
                z_i[i] = ldexp(cimag(a), -500/m);
            }
        }
    }
}

