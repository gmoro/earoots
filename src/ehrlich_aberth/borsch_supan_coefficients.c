// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <string.h>

void ea_borsch_supan_coefficients(const double* restrict in_r, const double* restrict in_i, int n, double* out_r, double* out_i)
{
    memset(out_r, 0, n*sizeof(double));
    memset(out_i, 0, n*sizeof(double));
    for(int i=1; i<n; i++) {
        for(int j=0; j<n-i; j++) {
            double x,y,s;
            x = in_r[j] - in_r[j+i];
            y = in_i[j] - in_i[j+i];
            s = 1/(x*x+y*y);
            x = x*s;
            y = -y*s;
            out_r[j] += x;
            out_i[j] += y;
            out_r[j+i] -= x;
            out_i[j+i] -= y;
        }
    }
}
