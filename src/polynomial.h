// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

// Polynomials transform
void ea_derivative(const double* p_r, const double* p_i, int n, double* dp_r, double* dp_i);
void ea_dd_reciprocal(const double* p_r, const double* p_i, int n, double* Rp_r, double* Rp_i);
void ea_d_reciprocal(const double* p, int n, double* Rp);
void ea_dd_mul_z(const double* p_r, const double* p_i, int n, double* zp_r, double* zp_i);
void ea_d_mul_z(const double* p, int n, double* zp);

// Single and double precision horner for real and complex numbers
void ea_d_horner(const double* coefficients, int n, const double* values, int m,
                 double* results);
void ea_dc_horner(const double* coefficients_r, const double* coefficients_i, int n,
                  const double* values_r, const double* values_i, int m,
                  double* results_r, double* results_i);

#endif

