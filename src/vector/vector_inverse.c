// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

void ea_vector_inverse( double* z_r, double* z_i, int m, double* iz_r, double* iz_i)
{
    for(int i=0; i<m; i++) {
        double s;
        s = 1/(z_r[i]*z_r[i] + z_i[i]*z_i[i]); 
        iz_r[i] = z_r[i]*s;
        iz_i[i] = -z_i[i]*s;
    }
}
