// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <math.h>
#include <string.h>

static void d_swap(double* a, double* b)
{
    double t;
    t = *a;
    *a = *b;
    *b = t;
}

int ea_vector_partition_pivot(double pivot, double* z_r, double* z_i, double* az, int m)
{
    if(isinf(pivot)) {
        return m;
    }       
    int c = 0;
    for(int i=0; i<m; i++) {
        if(az[i] > pivot) {
            c++;
        } else if(c>0) {
            d_swap(z_r + i-c, z_r + i);
            d_swap(z_i + i-c, z_i + i);
            d_swap(az  + i-c, az  + i);
        }
    }             
    return m-c;
}
