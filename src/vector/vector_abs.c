// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <math.h>

// Computes the modules of complex numbers
void ea_vector_abs(const double* in_r, const double* in_i, int n,
                   double* out)
{
    for(int i=0; i<n; i++) {
        out[i] = hypot(in_r[i], in_i[i]);
    }
}
