// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

void ea_vector_max2(const double* in_0, const double* in_1, int n,
                    double* out)
{
    for(int i=0; i<n; i++) {
        out[i] = (in_1[i]>in_0[i])?in_1[i]:in_0[i];
    }
}

