// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <math.h>

double ea_vector_max(const double* v, int m)
{
    double res = -INFINITY;
    for(int i=0; i<m; i++) {
        if(v[i]>res) {
            res = v[i];
        }
    }
    return res;
}
