#include "vector.h"
#include <stdarg.h>

void ea_vector_print(FILE* out, int verbose, const char* name, int step, int m, int nargs, ...)
{
    if(verbose > 1) {
        fprintf(out, "%s\n", name);
        va_list args;
        for(int i=0; i<m; i++) {
            va_start(args, nargs);
            fprintf(out, "  %5d.%5d:", step, i);
            for(int j=0; j<nargs; j++) {
                double* f = va_arg(args, double*);
                fprintf(out, " % .3e", f[i]);
            }
            va_end(args);
            fprintf(out, "\n");
        }
    }
}


