// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#ifndef EHRLICH_ABERTH_H
#define EHRLICH_ABERTH_H

// Borsch-Supan correction coefficient
void ea_borsch_supan_coefficients(const double* restrict in_r, const double* restrict in_i, int n, double* out_r, double* out_i);

// Newton correction coefficient
void ea_ehrlich_aberth_update(const double* vp_r, const double* vp_i,
                              const double* vdp_r, const double* vdp_i,
                              const double* bs_r, const double* bs_i, int m,
                              double* z_r, double* z_i);

#endif

