// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

void ea_derivative(const double* p_r, const double* p_i, int n, double* dp_r, double* dp_i)
{
    for(int i=0; i<n-1; i++) {
        dp_r[i] = (i+1)*p_r[i+1];
        dp_i[i] = (i+1)*p_i[i+1];
    }
}
