// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

void ea_dd_mul_z(const double* p_r, const double* p_i, int n, double* zp_r, double* zp_i)
{
    for(int i=0; i<n; i++) {
        zp_r[n-i] = p_r[n-1-i];
        zp_i[n-i] = p_i[n-1-i];
    }
    zp_r[0] = 0;
    zp_i[0] = 0;
}

void ea_d_mul_z(const double* p, int n, double* zp)
{
    for(int i=0; i<n; i++) {
        zp[n-i] = p[n-1-i];
    }
    zp[0] = 0;
}
