// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <string.h>

// simd parameters
//   Rbits size of registers
//   Nr number of registers
#ifdef __AVX512CD__
#define Rbits 512
#define Nr 32
#elif __AVX__
#define Rbits 256
#define Nr 16
#elif __SSE__
#define Rbits 128
#define Nr 16
#else
#define Rbits 64
#define Nr 16
#endif
#define Rbytes (Rbits/8)
#define Nd (Rbytes/8)
#define DBlock (Nd*Nr/2)
#define CSBlock (Nd*Nr/2)
#define CDBlock (Nd*Nr/4)

void ea_d_horner(const double* coefficients, int n, const double* values, int m,
              double* results)
{
    for(int i=0; i < m; i+=DBlock) {
        double d[DBlock] = {0};
        double v[DBlock] = {0};
        int p = (i+DBlock<m)?DBlock:(m%DBlock);
        memcpy(v, values+i, p*sizeof(double));
        for(int j=0; j<n; j++) {
            double c = coefficients[n-1-j];
            for(int k=0; k<DBlock; k++) {
                #pragma STDC FP_CONTRACT ON
                d[k] = d[k]*v[k]+c;
            }
        }
        memcpy(results+i, d, p*sizeof(double));
    }
}

void ea_dc_horner(const double* coefficients_r, const double* coefficients_i, int n,
               const double* values_r, const double* values_i, int m,
               double* results_r, double* results_i)
{
    for(int i=0; i < m; i+=CDBlock) {
        double x[CDBlock] = {0};
        double y[CDBlock] = {0};
        double u[CDBlock] = {0};
        double v[CDBlock] = {0};
        int p = (i+CDBlock<=m) ? CDBlock : (m%CDBlock);
        memcpy(u, values_r+i, p*sizeof(double));
        memcpy(v, values_i+i, p*sizeof(double));
        for(int j=0; j<n; j++) {
            double a = coefficients_r[n-1-j];
            double b = coefficients_i[n-1-j];
            for(int k=0; k<CDBlock; k++) {
                #pragma STDC FP_CONTRACT ON
                double s,t;
                s = a + u[k]*x[k] - v[k]*y[k];
                t = b + u[k]*y[k] + v[k]*x[k];
                x[k] = s;
                y[k] = t;
            }
        }
        memcpy(results_r+i, x, p*sizeof(double));
        memcpy(results_i+i, y, p*sizeof(double));
    }
}


#undef CDBlock
#undef CSBlock
#undef DBlock
#undef Nd
#undef Rbytes
#undef Nr
#undef Rbits
