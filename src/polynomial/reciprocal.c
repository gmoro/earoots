// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

void ea_dd_reciprocal(const double* p_r, const double* p_i, int n, double* Rp_r, double* Rp_i)
{
    for(int i=0; i<n; i++) {
        Rp_r[i] = p_r[n-1-i];
        Rp_i[i] = p_i[n-1-i];
    }
}

void ea_d_reciprocal(const double* p, int n, double* Rp)
{
    for(int i=0; i<n; i++) {
        Rp[i] = p[n-1-i];
    }
}
