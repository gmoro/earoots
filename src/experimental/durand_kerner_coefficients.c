/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/
#include "utils.h"
#include <complex.h>

void d_durand_kerner_coefficients(double pr, double pi, const double* restrict in_r, const double* restrict in_i, int m, int n_piv, double* out_r, double* out_i)
{
    for(int i=0; i<n_piv; i++) {
        out_r[i] = pr;
        out_i[i] = pi;
    }
    for(int i=0; i<m; i++) {
        int start=0;
        if(i<n_piv) {
            for(int j=0; j<i; j++) {
                double complex p;
                p = ((in_r[j]-in_r[i]) + I*(in_i[j]-in_i[i])) * (out_r[j] + I*out_i[j]);
                out_r[j] = creal(p);
                out_i[j] = cimag(p);
            }
            start = i+1;
        }
        for(int j=start; j<n_piv; j++) {
            //double x,y,re,im;
            //x = in_r[j] - in_r[i];
            //y = in_i[j] - in_i[i];
            //re = out_r[j]*x - out_i[j]*y;
            //im = out_i[j]*x + out_r[j]*y;
            //out_r[j] = re;
            //out_i[j] = im;
            double complex p;
            p = ((in_r[j]-in_r[i]) + I*(in_i[j]-in_i[i])) * (out_r[j] + I*out_i[j]);
            out_r[j] = creal(p);
            out_i[j] = cimag(p);
        }
    }
}

void d_durand_kerner_reciprocal_coefficients(double pr, double pi, const double* restrict in_r, const double* restrict in_i, double* dr, double* di, int m, int n_piv, double* out_r, double* out_i)
{
    for(int i=0; i<m; i++) {
        double complex c = 1/(in_r[i] + I*in_i[i]);
        dr[i] = creal(c);
        di[i] = cimag(c);
    }
    for(int i=n_piv; i<m; i++) {
        double complex c = (pr + I*pi)/(in_r[i] + I*in_i[i]);
        out_r[i] = creal(c);
        out_i[i] = cimag(c);
    }
    for(int i=0; i<m; i++) {
        int start = n_piv;
        if(i>=n_piv) {
            for(int j=n_piv; j<i; j++) {
                double x,y,re,im;
                x = 1 - (in_r[i]*dr[j] - in_i[i]*di[j]);
                y = 1 - (in_r[i]*di[j] + in_i[i]*dr[j]);
                re = out_r[j]*x - out_i[j]*y;
                im = out_i[j]*x + out_r[j]*y;
                out_r[j] = re;
                out_i[j] = im;
            }
            start = i+1;
        }
        for(int j=start; j<m; j++) {
            double x,y,re,im;
            x = 1 - (in_r[i]*dr[j] - in_i[i]*di[j]);
            y = 1 - (in_r[i]*di[j] + in_i[i]*dr[j]);
            re = out_r[j]*x - out_i[j]*y;
            im = out_i[j]*x + out_r[j]*y;
            out_r[j] = re;
            out_i[j] = im;
        }
    }
}
