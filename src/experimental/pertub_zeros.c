/****************************************************************************
        Copyright (C) 2022 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#include "utils.h"
#include <math.h>
#include <complex.h>

void perturb_zeros(double* z_r, double* z_i, int m)
{
    int num_zeros = 0;
    double pi = acos(-1);
    for(int k=0; k<m; k++) {
        if((z_r[k] == 0) && (z_i[k] == 0)) {
            num_zeros++;
        }
    }
    if(num_zeros>0) {
        int j = 0;
        double epsilon = ldexp(1, -1000/num_zeros);
        for(int k=0; k<m; k++) {
            if((z_r[k] == 0) && (z_i[k] == 0)) {
                complex double r = epsilon * cexp(2*pi*j/num_zeros*I);
                z_r[k] = creal(r) ;
                z_i[k] = cimag(r) ;
                j++;
            }
        }
    }
}
