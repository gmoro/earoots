// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include "error_bound.h"
#include "polynomial.h"
#include <math.h>

// Error analysis derived from Higham, Section 5.1
// TODO: handle underflow with absolute error, warning, or nan in disk radii
void ea_horner_error_bound(const double* abs_p, int n, const double p_error_ulp,
                           const double* abs_z, int m, const double v_error_ulp,
                           double* results)
{   
    double re_vc, re_c, re_h, re_ah, re;
    // relative error bound due to the input values and coefficients error bound
    re_vc = nextafter((n-1) * v_error_ulp, INFINITY);
    re_vc = nextafter(re_vc + p_error_ulp, INFINITY);
    
    // relative error for the multiplication of complex numbers
    // Brent, Percival, Zimmermann article
    re_c = nextafter(sqrt(5),INFINITY);

    // relative error bound due to operation approximations in horner
    re_h = nextafter(2*(n-1) * re_c, INFINITY);

    // relative error due to absolute value approximation
    re_ah = nextafter(re_h + 2, INFINITY);

    // total error relative to evaluation with modules of coefficients and z
    re = ea_gamma2(nextafter(re_vc + re_ah, INFINITY));

    // evaluation with modules of coefficients and values
    ea_d_horner(abs_p, n, abs_z, m, results);

    // relative error on evaluation with modules of coefficients and z
    // is gamma(2(n-1)) for horner and gamma(2(n-1)+2) for the
    // approximation of the absolute values. Since in our gamma the
    // denominator is multiplied by 2 and re_h > 2(n-1)sqrt(5)+2 > 4(n-1)+2,
    // this error is covered by gamma2(re_h)

    // apply error bounds
    for(int i=0; i<m; i++) {
        results[i] = nextafter(results[i] * re, INFINITY);
    }
}

