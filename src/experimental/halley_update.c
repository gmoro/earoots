#include <complex.h>

void cd_halley_update(const double* vp_r, const double* vp_i,
                      const double* vdp_r, const double* vdp_i,
                      const double* vddp_r, const double* vddp_i, int m,
                      double* z_r, double* z_i)
{
    for(int i=0; i<m; i++) {
        if((vdp_r[i] != 0) || (vdp_i[i] != 0)) {
            complex double a;
            a = (vp_r[i] + I*vp_i[i])/(vdp_r[i] + I*vdp_i[i] - 0.5*(vp_r[i] + I*vp_i[i]) * (vddp_r[i] + I*vddp_i[i]) / (vdp_r[i] + I*vdp_i[i]));
            z_r[i] -= creal(a);
            z_i[i] -= cimag(a);
        }
    }
}

