#include "utils.h"
#include <stdarg.h>

void init_arrays(double* pool, int n, int num_arrays, ...)
{
    va_list args;
    va_start(args, num_arrays);
    for(int i=0; i<num_arrays; i++) {
        *va_arg(args, double**) = pool + i*n;
    }
}
