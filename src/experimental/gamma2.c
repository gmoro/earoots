// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#include <math.h>

double ea_gamma2(double e_ulp)
{
    double num, den;
    num = ldexp(e_ulp, -53);
    den = nextafter(1-2*num, -INFINITY); // times 2 with respect to approximate value
    return nextafter(num/den, INFINITY);
}
