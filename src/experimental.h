// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#ifndef EXPERIMENTAL_H
#define EXPERIMENTAL_H

#ifdef __cplusplus
extern "C" {
#endif

// Perturb complex numbers equal to zero
void perturb_zeros(double* z_r, double* z_i, int m);

// Durand-Kerner correction coefficient
void d_durand_kerner_coefficients(double pr, double pi, const double* restrict in_r, const double* restrict in_i, int n, int piv, double* out_r, double* out_i);
void d_durand_kerner_reciprocal_coefficients(double pr, double pi, const double* restrict in_r, const double* restrict in_i, double* dr, double* di, int m, int n_piv, double* out_r, double* out_i);

// Newton correction coefficient
void cd_halley_update(const double* vp_r, const double* vp_i,
                      const double* vdp_r, const double* vdp_i,
                      const double* vddp_r, const double* vddp_i, int m,
                      double* z_r, double* z_i);
void cd_newton_update(const double* vp_r, const double* vp_i,
                      const double* vdp_r, const double* vdp_i, int m,
                      double* z_r, double* z_i);

// Bound the error on the value of Horner evaluation
void ea_horner_error_bound(const double* abs_up_p, int n, const double p_error_ulp,
                           const double* abs_up_z, int m, const double v_error_ulp,
                           double* results);

// Relative error with respect to the output
double ea_gamma2(double e_ulp);

#ifdef __cplusplus
}
#endif
#endif

