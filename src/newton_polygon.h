// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#ifndef NEWTON_POLYGON_H
#define NEWTON_POLYGON_H

// Convex hull
int ea_lower_convex_hull(const double* px, const double* py, int n, int* result);

// Newton polygon using convex hull
int ea_newton_polygon(const double* p_r, const double* p_i, const double* ep, int n, double* tmp_x, double* tmp_y, int* np);

// Initial root estimations based on the Newton polygon
void ea_initial_values(const double* p_r, const double* p_i,
                       const int* np, int m, double* z_r, double* z_i);

// Pivot value from Newton polygon to reduce overflow
double ea_get_pivot(const double* ap, int n, const int* np, int m);

// Trim front and back zeros
int ea_trim_zeros(const double* p, const double* in_rads, int n, double* z, double* rad, int* fz, int* bz);

#endif

