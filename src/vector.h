// Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

#ifndef VECTOR_H
#define VECTOR_H

#include <stdio.h>

// Computes the modules of complex numbers
void ea_vector_abs(const double* in_r, const double* in_i, int n,
                          double* out);

// Computes the pairwise max of 2 array of numbers in double precision
void ea_vector_max2(const double* in_0, const double* in_1, int n,
                           double* out);

// Computes the max of an array of numbers in double precision
double ea_vector_max(const double* v, int m);

// Computes the inverse of complex numbers
void ea_vector_inverse(const double* z_r, const double* z_i, int m, double* iz_r, double* iz_i);

// Partitions an array with respect to a pivot value
int ea_vector_partition_pivot(double pivot, double* z_r, double* z_i, double* az, int m);

// Print for debugging
void ea_vector_print(FILE* out, int verbose, const char* name, int step, int m, int nargs, ...);

#endif
