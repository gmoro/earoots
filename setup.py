# Copyright 2022 Guillaume Moroz, Inria <guillaume.moroz@inria.fr>

from numpy.distutils.core import setup, Extension
from numpy.distutils.command.build_ext import build_ext
from numpy.distutils.command.build import build
import glob
import os

c_directories = ['ehrlich_aberth',
                 'error_bound',
                 'newton_polygon',
                 'polynomial',
                 'vector']

c_files = [os.sep.join(['src', 'earoots.c']),
           os.sep.join(['earoots', 'interface.c'])]

for d in c_directories:
    c_pattern = os.sep.join(['src',d, '*.c'])
    c_files += glob.glob(c_pattern)

class build_ext_unix_flags(build_ext):
    def build_extension(self, ext):
        if self.compiler.compiler_type == 'unix':
            ext.extra_compile_args = ['-fno-wrapv', '-g0', '-O2',                             # to cancel python flags
                                      '-march=native', '-ftree-vectorize', '-funroll-loops',  # for optimization
                                      '-frounding-math',                                      # for correct fp exception detection
                                      '-Wno-unknown-pragmas']
        build_ext.build_extension(self, ext)


libearoots = Extension('earoots._interface', c_files,
                      include_dirs = ['src'],
                      extra_link_args = ['-lm'])

setup(name = 'earoots',
      version = '0.13',
      description = 'Ehlrich-Aberth complex root finder',
      author='Guillaume Moroz',
      author_email='guillaume.moroz@inria.fr',
      python_requires = '>=3',
      install_requires = ['numpy'],
      url='https://gitlab.inria.fr/gmoro/earoots',
      ext_modules = [libearoots],
      cmdclass = {'build_ext': build_ext_unix_flags},
      packages    = ['earoots'])


